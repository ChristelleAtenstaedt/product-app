import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()
export class ProductService {
  constructor(private http: HttpClient) {}

  getProductItems(): Observable<Product[]> {
    return this.http.get<Product[]>('assets/products.json');
  }

  getProductById(productId: string): Observable<Product | undefined> {
    return this.http.get<Product[]>('/assets/products.json').pipe(
      map((products: Product[]) => {
        return products.find((product) => product.id === productId);
      })
    );
  }
}

export interface Product {
  id: string;
  type: string;
  name: string;
  BBAN: string;
  IBAN: string;
  currency: string;
  available: boolean;
}
