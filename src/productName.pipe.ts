import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productName',
})
export class ProductNamePipe implements PipeTransform {
  transform(value: string): string {
    // Split the string by camel case and join with spaces
    return value.replace(/([A-Z])/g, ' $1').trim();
  }
}
